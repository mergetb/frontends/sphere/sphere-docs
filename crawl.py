# Crawl remote or local docs and detect broken links
# could do the same for images, but currently not done

from bs4 import BeautifulSoup, SoupStrainer
import requests, re, sys
from urllib.parse import urlparse

class Page:
    url = ""
    parenturl = ""
    
url = "http://localhost:1313/docs/"

requests_session = requests.session()
mylist = []
crawled = []
myurls = []
p = Page()
p.url = url
mylist.append(p)
fourfours = dict()
broken = 0

while True:
    elem = mylist.pop()
    myurl = elem.url
    myurl = myurl.replace("https://", "http://")
    print("Crawling ", myurl)
    try:
        page = requests.get(myurl)
    except Exception as ex:
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        message = template.format(type(ex).__name__, ex.args)
        print(message)
    # Only crawl stuff once
    crawled.append(myurl)
    response_code = str(page.status_code)
    data = page.text
    soup = BeautifulSoup(data, features="html.parser")
    if int(response_code) != 200 and myurl not in fourfours.keys():
        # This is where we detect missing pages, so grep for Wrong in output to find out which pages to fix
        print("Wrong response code ", myurl, " code ", response_code, " parent ", elem.parenturl)
        broken = 1
        fourfours[myurl] = 1
        continue
    for link in soup.find_all('a'):
        newurl = link.get('href')
        if newurl is None:
            continue
        myclass = None
        if link.has_attr('class'):
            if link['class'] and (link['class'][0].startswith('reference') or link['class'][0].startswith('header')):
                continue
        # Only follow links that are ours
        if (newurl.startswith("http://merge") or newurl.startswith("https://merge")):
            if (newurl in crawled or newurl in myurls):
                continue
            p = Page()
            p.parenturl = myurl
            p.url = newurl                    
            mylist.append(p)
            myurls.append(newurl)
        # Rebuild local links
        elif re.match(r"^\/\w+", newurl):
              parse_object = urlparse(myurl)
              base = "https://" + parse_object.netloc
              wnewurl = base + newurl
              if (wnewurl in crawled or wnewurl in myurls):
                  continue
              p = Page()
              p.parenturl = myurl
              p.url = wnewurl
              mylist.append(p)
              myurls.append(wnewurl)
        elif (newurl.startswith("..")): 
            parse_object = urlparse(myurl)
            base = "https://" + parse_object.netloc + parse_object.path
            wnewurl = base + newurl
            if (wnewurl in crawled or wnewurl in myurls):
                continue
            p = Page()
            p.parenturl = myurl
            p.url = wnewurl
            mylist.append(p)
            myurls.append(wnewurl)
        else:
            continue
       
    if (len(mylist) == 0):
        break

print("Broken ", broken)
sys.exit(broken)
