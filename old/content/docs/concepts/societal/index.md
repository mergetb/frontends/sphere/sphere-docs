---
title: "Societal"
weight: 1
# geekdocFlatSection: false
# geekdocToc: 6
# geekdocHidden: false
---

* Our nation depends on correct and reliable functioning of network and computing systems
* Frequency and impact of cybersecurity and privacy attacks are constantly increasing:
    * Solar Winds supply-chain attack, which exposed confidential government data
    * Colonial Pipeline attack, which shut down our major gas pipeline for several days
    * Ransomware attacks more than tripled
    * DDoS attacks doubled
    * Data breaches increased by 70%
* **Research progress in cybersecurity and privacy is of critical national importance to ensure safety of U.S. people, infrastructure and data.**
