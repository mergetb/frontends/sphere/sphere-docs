---
title: "Research Infrastructure"
weight: 40
# geekdocFlatSection: false
# geekdocToc: 6
# geekdocHidden: false
---

- Diverse hardware to support diverse research needs (85% of today's publications):
    - General and embedded compute notes with trusted hardware
    - PLCs and IoT devices, programmable switches and NICs
    - GPU-equipped nodes

- Six user portals supporting:
    - Exploratory research (MAN)
    - Novice users (GUI)
    - Mature research (JUP)
    - Use in classes (EDU)
    - Use in human user studies (HUM)
    - Use for artifact evaluation (AEC)

- Flexible security policies:
    - Full isolation
    - Measurement research
    - Software download
    - Risky experiments with malware

- Libraries of artifacts
    - REEs and other artifacts
    - Easy reuse on SPHERE Research Infrastructure
