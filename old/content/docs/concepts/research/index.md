---
title: "Research"
weight: 10
# geekdocFlatSection: false
# geekdocToc: 6
# geekdocHidden: false
description: > 
    SPHERE research concepts
---

The cybersecurity and privacy research community needs a common, rich, representative research infrastructure, which meets the needs across all members of the community, and facilitates reproducible science.


* Common, rich infrastructure:
    * Security and privacy issues affect different technologies differently (e.g., different CPU architectures)
    * Some emerging technology can create new vulnerabilities (e.g., IoT )
    * New technologies can be used for defense (e.g., trusted hardware, SDN )
    * Infrastructure must have diverse hardware to meet wide research needs
* Meet needs across all members of the community
    * Experienced and novice users, researchers and students
* Facilitate reproducible science:
    * Help researchers create, share, and reuse research artifacts
