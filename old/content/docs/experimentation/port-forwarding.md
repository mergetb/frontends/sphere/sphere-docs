---
title: "Port Forwarding"
linkTitle: "Port Forwarding"
weight: 21
description: >
  Doing port fowarding on Merge testbeds
---

{{% hint title="Attention" type="warning" %}}

This guide assumes you are using the reference portal at `mod.deterlab.net`. Instructions on this page only apply to that portal.

{{% /hint %}}

# Operating System Requirements

Before port forwarding can be done on Deterlab, different operating systems have different requirements. Port forwarding is only possible on Linux at the moment, so a workaround is required for Windows and MacOS users.

**Windows:** WSL (Windows Subsystem for Linux), which can be found from the Microsoft Store. Make sure to download Ubuntu as the distribution. **Note:** Even though the download below has a Windows installation, it cannot be used to SSH into the XDC. The mrg commands must be run within the same directory as the mrg file, but it will not let the user SSH until support is added.
       
**MacOS:** VirtualBox with Ubuntu. **Note:** The lab must be done entirely inside of the VirtualBox. All of the browser navigations must be done within the virtual machine.

# Application Requirements

The instructions assume you have installed `mrg` utility on your home machine. To do so, please obtain the source (and follow README to compile it) or binary <a href="https://gitlab.com/mergetb/portal/cli/-/releases">from this site</a>.

The instructions also assume that you have <a href="../getting-started/#configuring-the-api-endpoint">configured your API point</a> (you only have to do this once) and have logged into your merge account by doing `mrg login yourusername -p yourpassword` (you have to do this each time you open a new terminal on your machine). 

# Port Forwarding Steps

Make sure that you have realized and materialized your experiment (e.g., you have run `startexp labname`) and that you have set it up (i.e., you have ran `runlab labname`).

To do the actual port forwarding between a port `yourport` on your machine and node `yournode` in an experiment within `yourproject` via an XDC named `yourxdc`, you need to SSH with the following command:
```
mrg xdc ssh yourxdc.yourproject -L yourport:yournode:80
```
So, to port forwarding onto 8080 with the `pathname` lab, you would type this:
```
mrg xdc ssh xdc.yourusername -L 8080:pathname:80
```

