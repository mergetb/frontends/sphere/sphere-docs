---
title: "Command Line Interface Reference"
linkTitle: "CLI Reference"
weight: 4
description: >
  Using the Merge command line interface
---

{{% hint title="Tips" type="primary" %}}

- All commands support **abbreviated syntax**, so the following are equivalent

```sh
mrg list experiments
```

```sh
mrg li e
```

- As of version `v1.1.1`, the `mrg` binary supports self updating via `mrg update binary`.

- Most commands support **rendering as JSON** via the `--json` flag. This is useful
  for tool and script integration.

- All **names have a hierarchical syntax**, so an experiment called `muffins` in the
  project `bakery` is referred to as `muffins.bakery`

- There are **useful help menus** with all commands accessible through the `-h`
  flag.

- **Be sure to login first** `mrg login <username> <password>`, login tokens do
  not last forever and may expire so you may have to login again from time to
  time.
  {{% /hint %}}

## Introduction

The command line interface to Merge, named `mrg`, lets users interact with a single Merge Portal.
To use most commands, users must first login to the testbed and authenticate against an existing active account. 
The exception for this is account registration when, of course, one cannot authenticate using an active account
as it does not exist. Once authenticated, the following commands  are available (as of version `v1.1.15`):

```
The MergeTB CLI Tool

Usage:
  mrg [command]

Available Commands:
  activate      Activate a user or organization
  compile       Compile a model. Return success or compile error stack on failure.
  completion    Generate the autocompletion script for the specified shell
  config        Manage CLI configuration
  delete        Delete things
  dematerialize Dematerialize a realization
  facility      Facility specific commands
  freeze        Freeze a user or organization
  generate      Generate files or configurations pertaining to a realization
  harbor        Harbor management
  help          Help about any command
  init          Initialize a user
  list          List things
  login         Log in to a portal
  logout        Logout the currently logged in user from the portal
  materialize   Materialize a realization
  membership    Membership commands
  new           Make new things
  pools         Pool specific commands
  push          Push a model to the given experiment repository
  realize       Realize an experiment
  register      Register a new identity with the portal
  relinquish    Relinquish a realization
  show          Show information about things
  unregister    Unregister an existing identity from the portal
  update        Update existing things
  whoami        Show the identity of the logged in user
  xdc           XDC tools

Flags:
  -c, --colorless         Disable color output
  -h, --help              help for mrg
  -j, --json              Output JSON
  -l, --loglevel string   Level to log at. One of panic, fatal, error, warning, info, debug, trace (default "info")
  -s, --server string     MergeTB API server address
  -v, --version           version for mrg

Use "mrg [command] --help" for more information about a command.
```

All commands support a `-h` or `--help` argument which shows the usage and help for that specific command. 

At execution all commands have a policy applied before they are executed. Based on the account privileges of the user
running the command the action will be allowed or denied. Most commands also are aware of the caller's data in the system. 
For instance, when listing projects via `mrg list projects`, only the callers projects will be listed. Other data returned from a call 
may be filtered to match the security level of the user and the data accessed. 

## Accounts

### Register

To use Merge, an account is needed. To register for an account use the `register` command.

#### Usage
```
$ mrg register --help
Register a new identity with the portal

Usage
  mrg register <username> <email> <institution> <category> [flags]

Flags:
      --country string             user country
  -h, --help                       help for register
  -n, --name string                user full name
  -o, --organization string        request organization membership
  -r, --organization_role string   request organization role (one of Member,Maintainer,Creator) (default "Member")
  -p, --passwd string              user password
      --usstate string             user US state

Global Flags:
  -c, --colorless         Disable color output
  -j, --json              Output JSON
  -l, --loglevel string   Level to log at. One of panic, fatal, error, warning, info, debug, trace (default "info")
  -s, --server string     MergeTB API server address
```

To register an account the following are required: username, full name, user institution, an account category, a password, and the user's country.
(If the user's country is the United States, then a US State is also needed.)

#### Example

```
mrg register olive olive@example.com "University of San Francisco" "Graduate student" --name "Olive Oyl" --country "United States" --usstate California -p 123abc! 
```

If the password is not given, `mrg` will prompt for it. 

The list of institutions and user categories are specific to a Merge portal installation. To see the list of available entries, use the commands 
`mrg list portal institutions` and `mrg list portal user-categories`. (These commands do not require authentication.) 
If a required argument is left blank or is unacceptable, `mrg` will list the available options. For example here is the 
output when `XXXX` is given as a user category:

```
$ mrg register olive olive@example.com CENIC XXXX --name "Olive Oyl" --country Albania -p abc123!
Error:      InvalidArgument
Message:    User category must be one of Other or Graduate student, Professor, Researcher/Scientist, Teaching assistant, Undergraduate student
```

`Other` is usually configured to be an acceptable argument as well, but it depends on the Merge portal administration configuring it to be so.

Once an account is registered, the Merge portal operators need to approve and active the account. 

### Login

Logging into a Merge portal is done, as one would think, via the `login` command. This takes the username and password of the account. If
the password is not given, `mrg` will prompt for it. On success, a time-limited token will be granted to the user. While the token is active, 
all other commands can be run.

#### Usage
```
$ mrg login --help
Log in to a portal

Usage:
  mrg login <username> [flags]

Flags:
  -h, --help            help for login
      --nokeys          Do not copy merge generated keys and certificate into ~/.ssh
  -p, --passwd string   User password

Global Flags:
  -c, --colorless         Disable color output
  -j, --json              Output JSON
  -l, --loglevel string   Level to log at. One of panic, fatal, error, warning, info, debug, trace (default "info")
  -s, --server string     MergeTB API server address
```

#### Example

```
$ mrg login olive
Enter password for olive:
$
```

No response indicates success.

A failure:
```
$ mrg login olive -p notcorrect
Error:      PermissionDenied
Message:    The provided credentials are invalid, check for spelling mistakes in your password or username, email address, or phone number.
```


## Experiments

### Listing

```
mrg list experiments
```

Example:

```sh
$ mrg list exp
Name.Project          Description    Mode
------------          -----------    ----
experiment.project                   Public
exp.glawler                          Public
bbb.glawler                          Public
eee.glawler                          Public
```

### Showing

```sh
mrg show experiment <name>.<project>
```

Usage:

```sh
Show an experiment

Usage:
  mrg show experiment <experiment>.<project> [flags]

Flags:
  -h, --help          help for experiment
  -m, --with-models   Save models for each revision
  -S, --with-status   Show compilation status for each revision

```

Example:

```sh
$ mrg show experiment experiment.project
Repo: https://git.dorvan.mergetb.net/project/experiment
Mode: Public
Description:
Creator: glawler
Maintainers:
Realizations:
  Revision                                    Realizations
  --------                                    ------------
  9eabb626da475f542bf4002cc2d9d1c448f6cafa    rlz.experiment.project
  d3da9590bbc8bedcb6c494ca6d84b57edb1aa9d6    (none)
```

## Models

A model is an experiment topology and is read from an experiment's git repository. The
model syntax is documented [here]({{<ref "model-ref" >}}).

### Compiling

A model can be compiled before pushing to an experiment repository via the `compile` command.
If successful the compiled model will be printed to standard out. On failure the
compilation errors will.

Note that this does not push the model to the experiment repository. It is just informational.
To push use `mrg push` or `git push`.

```sh
Compile a model. Return success or compile error stack on failure.

Usage:
  mrg compile <model-file> [flags]

Flags:
  -h, --help    help for compile
  -q, --quiet   Do not show the compiled model on success.

```

Successful example (`-q` argument is given to quiet output)

```sh
glawler@river:~$ mrg compile -q model.py
glawler@river:~$ echo $?
0
```

Unsuccessful example:

```sh
glawler@river:~$ mrg compile model.py
Title:        MX failed to compile
Detail:       Compiliation Failure
Evidence:     Exception: 'Network' object has no attribute 'Connect'

Type:          https://mergetb.org/errors/mx-failed-to-compile
Timestamp:     2022-02-14 20:04:11.560308413 +0000 UTC m=+5998.900641627
```

### Pushing

A model can be pushed to an experiment via `git` or `mrg`. To push via `mrg` use the
`mrg push ...` command.

```sh
Push a model to the given experiment repository.

Usage:
  mrg push <model-file> <experiment>.<project> [flags]

Flags:
      --branch string   Repository branch to push the model to. (default "master")
  -h, --help            help for push
      --tag string      Tag the commit with the given tag.
```

For example to push the model in `./model.py` to the master branch in experiment
`hello.murphy`:

```sh
mrg push ./model.py hello.murphy
Push succeeded. Revision: fb90c2aa1a19a07410d39ad373c842e7ee544586
```

If `--tag` is given the revision will be tagged with the given tag:

```sh
mrg push ./model.py hello.murphy --tag v1.2.1
```

This creates a git tag on the revision and can be used to
realize the model.

{{% hint title="Note" type="warning" %}}
Although the `push` usage has a `branch` argument, the command only supports pushing to
the `master` branch. Pushing to a non-master branch will be in a future release.
{{% /hint %}}

## Realizations

### Realizing

```sh
mrg realize <realization>.<experiment>.<project> revision|tag <reference> [flags]
```

Here `<reference>` is the git revision or git tag of the source repository you would like to
realize. For example to create the realization "henry" with a revision tagged "olive" in
the experiment `hello` in the project `murphy`:

```sh
mrg realize henry.hello.murphy tag olive
```

The same but using the revision:

```sh
mrg realize henry.hello.murphy revision bbd67f119672bc0cedc8fd97476e6aeea7458a6c
```

### Listing

To list all realizations you have access to use `list `.

```sh
mrg list realizations
```

Example:

```sh
$ mrg list realizations
Realization               Nodes    Links    Status
-----------               -----    -----    ------
rlz.experiment.project    5        2        Succeeded
```

### Relinquishing

Relinquishing frees all resources held by a realization. If there is an active
materialization associated with the realization it will be dematerialized
before the realization is relinquished.

```sh
mrg relinquish <realization>.<experiment>.<project>
```

### Showing

```sh
mrg show realization <realization>.<experiment>.<project>
```

### Generating configuration

`mrg generate` allows you to generate configurations from a realization, including INI style Ansible
inventories and etchosts-style name-to-IP mappings:

```sh
mrg generate inventory <realization>.<experiment>.<project>
```

```sh
mrg generate etchosts -p exp- <realization>.<experiment>.<project>
```

## Materializations

### Listing

```sh
mrg list materializations
```

### Showing

```sh
mrg show materialization <realization>.<experiment>.<project>
```

## XDCs

### Listing

```sh
mrg list xdcs
```

### Attaching

```sh
mrg xdc attach <xdc>.<project> <realization>.<experiment>.<project>
```

### Detaching

```sh
mrg xdc detach <xdc>.<project>
```
